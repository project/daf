<?php

/**
 * @file
 * Enables modules and site configuration for Demo Framework site installation.
 */

/**
 * Implements hook_install_tasks().
 */
function daf_install_tasks() {
  $tasks = array(
      'daf_subprofiles' => array(
          'display_name' => st('Add-on Modules'),
          'type' => 'form',
      ),
  );
  return $tasks;
}

function daf_subprofiles() {
  $form = array(
      '#markup' => st('Select multiple options for modules needs to be installed.'),
  );
  $files = system_rebuild_module_data();
  $visible_files = $files;
  foreach ($visible_files as $filename => $file) {
    if (!empty($file->info['hidden'])) {
      unset($visible_files[$filename]);
    }
  }

  $module_list = array(
      'daf_dev_tools' => st('Development Tools'),
      'daf_spark' => st('Authoring / Admin Tools'),
      'daf_multilingual' => st('Multilingual Features'),
      'daf_workflow' => st('Workflow Features'),
      'daf_search' => st('Search Features'),
      'daf_analytics' => st('Analytics Features'),
      'daf_layout' => st('Layout Features'),
      'daf_responsive' => st('Responsive Features'),
      'daf_seo' => st('SEO Features'),
      'daf_security' => st('Security Features'),
  );
  $new_array = array();
  $header = array(t('Module'), t('Required Modules'));
  // If this module requires other modules, add them to the array.
  foreach ($module_list as $filename => $module_description) {
    $module = $visible_files[$filename];

    $extra = array();
    foreach ($module->requires as $requires => $v) {
      if (!isset($files[$requires])) {
        $extra['requires'][$requires] = t('@module (<span class="admin-missing">missing</span>)', array('@module' => drupal_ucfirst($requires)));
        $extra['disabled'] = TRUE;
      }
      // Only display visible modules.
      elseif (isset($visible_files[$requires])) {
        $requires_name = $files[$requires]->info['name'];
        // Disable this module if it is incompatible with the dependency's version.
        if ($incompatible_version = drupal_check_incompatibility($v, str_replace(DRUPAL_CORE_COMPATIBILITY . '-', '', $files[$requires]->info['version']))) {
          $extra['requires'][$requires] = t('@module (<span class="admin-missing">incompatible with</span> version @version)', array(
              '@module' => $requires_name . $incompatible_version,
              '@version' => $files[$requires]->info['version'],
          ));
          $extra['disabled'] = TRUE;
        }
        // Disable this module if the dependency is incompatible with this
        // version of Drupal core.
        elseif ($files[$requires]->info['core'] != DRUPAL_CORE_COMPATIBILITY) {
          $extra['requires'][$requires] = t('@module (<span class="admin-missing">incompatible with</span> this version of Drupal core)', array(
              '@module' => $requires_name,
          ));
          $extra['disabled'] = TRUE;
        } elseif ($files[$requires]->status) {
          $extra['requires'][$requires] = t('@module (<span class="admin-enabled">enabled</span>)', array('@module' => $requires_name));
        } else {
          $extra['requires'][$requires] = t('@module (<span class="admin-disabled">disabled</span>)', array('@module' => $requires_name));
        }
      }
    }
    $dependency = implode(",", $extra['requires']);
    $rows = array(
        array($module_description, $dependency),
    );
    $new_array[$filename] = $module_description . theme('table', array('header' => $header, 'rows' => $rows));
  }
  $form['subprofiles'] = array(
      '#title' => st('Select additional set of modules'),
      '#type' => 'checkboxes',
      '#options' => $new_array,
  );

  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => st('Continue'),
  );
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function daf_subprofiles_submit(&$form, $form_state) {
  $modules = array();
  $modules_list = array();
  $modules = $form_state['values']['subprofiles'];
  $modules_list = array_values(array_filter($modules));

  if (!empty($modules_list)) {
    module_enable($modules_list, TRUE);
    drupal_set_message('All required addons have been enabled.', 'status', FALSE);
  }
}

/**
 * Implements hook_install_tasks_alter().
 */
function daf_install_tasks_alter(&$tasks, $install_state) {
  global $install_state;

  // Skip profile selection step.
  $tasks['install_select_profile']['display'] = FALSE;

  // Skip language selection install step and default language to English.
  $tasks['install_select_locale']['display'] = FALSE;
  $tasks['install_select_locale']['run'] = INSTALL_TASK_SKIP;
  $install_state['parameters']['locale'] = 'en';
}
